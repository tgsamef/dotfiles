#!/usr/bin/env sh



if [ -f "$HOME/.zshrc" ]; then
    echo "updating .zshrc file..." && sleep 1 &&
    rsync --update -av ${HOME}/dotfiles/.zshrc ${HOME}/.zshrc &&
    echo done...
else
    echo "creating new .zshrc file..." && sleep 1 &&
    rsync -av ${HOME}/dotfiles/.zshrc ${HOME}/.zshrc &&
    echo done...
fi


if [ -f "$HOME/.vimrc" ]; then
    echo "updating .vimrc file..." && sleep 1 &&
    rsync --update -av ${HOME}/dotfiles/.vimrc ${HOME}/.vimrc &&
    echo done...
else
    echo "creating new .vimrc file..." && sleep 1 &&
    rsync -av ${HOME}/dotfiles/.vimrc ${HOME}/.vimrc &&
    echo done...
fi
