#
#   Alex P. ::  same-f (tg.same.f)             ___
#   src:    ::  http://gitlab.com/fgsamef    /'___\
#   mail:   ::  tg.same.f@gmail.com         /\ \__/
#    ___   _         ___  __ _ _ __ ___   __\ \ ,__\
#   [(_)] |=|   :   / __|/ _` | '_ ` _ \ / _ \ \ \_/
#    '_`  |_|   :   \__ \ (_| | | | | | |  __/\ \ \
#   /mmm/ _/    :   |___/\__,_|_| |_| |_|\___| \/_/
#
# zshrc just pretty standard stuff


# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi


unlimit
limit stack 8192
limit core 0
limit -s
umask 022

freload() { while (( $# )); do; unfunktion $1; autoload -U $1; shift; done }
fpath=($fpath ~/.zfunc)
for func in $^fpath/*(N-.x:t); autoload $func
typeset -U path cdpath fpath manpath
manpath="/usr/man:/usr/share/man:/usr/local/man:/usr/X11R6/man:/opt/qt/doc"

export MANPATH
export LESS="-R"
export TERM="xterm-256color"
export PATH="$HOME/bin:/usr/.local/bin:$PATH"
export ZSH="$HOME/.oh-my-zsh"
export LANG="ru_RU.UTF-8"
export LC_NUMERIC="POSIX"
export EDITOR="vim"
export VISUAL="vim"
export MANPAGER="sh -c 'col -bx | bat -l man -p'"
export ARCHFLAGS="-arch x86_64"
export RANGER_LOAD_DEFAULT_RC=false

if [ $TILIX_ID ] || [ $VTE_VERSION ]; then
        source /etc/profile.d/vte.sh
fi
if [ -z "$XDG_CONFIG_HOME" ]; then
	export XDG_CONFIG_HOME="$HOME/.config"
fi
if [ -z "$XDG_DATA_HOME" ]; then
	export XDG_DATA_HOME="$HOME/.local/share"
fi
if [ -z "$XDG_CACHE_HOME" ]; then
	export XDG_CACHE_HOME="$HOME/.cache"
fi

zstyle ':omz:update' frequency 3
zstyle ':omz:update' mode reminder
bindkey -v

HISTFILE=~/.zhistory
SAVEHIST=20000
HISTSIZE=20000
HIST_STAMPS="dd.mm.yyyy"
setopt APPEND_HISTORY
setopt HIST_IGNORE_ALL_DUPS
setopt HIST_IGNORE_SPACE
setopt HIST_REDUCE_BLANKS

setopt notify globdots correct pushdtohome cdablevars autolist
#setopt correctall autocd recexact longlistjobs
setopt autoresume histignoredups pushdsilent noclobber
setopt autopushd pushdminus extendedglob rcquotes mailwarning
setopt No_Beep
setopt IGNORE_EOF
unsetopt bgnice autoparamslash

zmodload -a zsh/stat stat
zmodload -a zsh/zpty zpty
zmodload -a zsh/zprof zprof
zmodload -ap zsh/mapfile mapfile

autoload -U compinit
compinit

DISABLE_UNTRACKED_FILES_DIRTY=true
ZSH_THEME="powerlevel10k/powerlevel10k"
CASE_SENSITIVE=true
ENABLE_CORRECTIONS=true
COMPLETION_WAITING_DOTS="%F{yellow}waiting...%f"
CFLAGS="-03 -march=pentium4 -fomit-frame-pointer -funroll-loops -pipe -mfpmath=sse -mmmx -msse2 -fPIC"
CXXFLAGS="$CFLAGS"
BOOTSTRAPCFLAGS="$CFLAGS"
export CFLAGS CXXFLAGS BOOTSTRAPCFLAGS

case ${TERM} in
	xterm*|rxvt*|st|urxvt|tilix)
		PROMPT_COMMAND='echo -ne "\033]0;${USER}@${HOSTNAME%%.*}:${PWD/#$HOME/\~}\007"'
		;;
	screen*)
		PROMPT_COMMAND='echo -ne "\033_${USER}@${HOSTNAME%%.*}:${PWD/#$HOME/\~}\033\\"'
		;;
esac


plugins=(git colored-man-pages docker
	emotty emoji screen perl)

alias less='less -M'
alias ispell='ispell -d russian'
alias ls='ls -F --color=auto'
alias ll='ls -l'
alias la='ls -A'
alias lla='ls -la'

. /usr/lib/python3.10/site-packages/powerline/bindings/zsh/powerline.zsh

source $ZSH/oh-my-zsh.sh

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh
